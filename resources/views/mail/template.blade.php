<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<header>
    <h2>Mail đăng ký tài khoản</h2>
</header>
<main>
    <b>Xin chào {{ $info->get('name') }}</b>
    <p>
        Bạn đã đăng ký thành công tài khoản có email: {{ $info->get('email') }}
    </p>
</main>
</body>
</html>
