@extends('layouts.layout')

@section('title', 'Import excel')

@section('title-heading', 'Import excel')

@section('content')
    <form action="{{ route('excel.store') }}" method="post" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="formFile" class="form-label">Default file input example</label>
            <input class="form-control" type="file" id="formFile" name="file-excel"/>
        </div>
        <div class="mt-2">
            <button class="btn btn-outline-primary" type="submit">Upload file</button>
        </div>
    </form>
@endsection
