@extends('layouts.layout')

@section('title', 'Import excel')

@section('title-heading', 'Alert')

@section('content')
    <div class="alert alert-primary d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
        <div>
            Import excel successfully
            @isset($users)
                <ul>
                @foreach($users as $user)
                    <li>{{ $user['email'] }}</li>
                @endforeach
                </ul>
            @endisset
        </div>
    </div>
@endsection
