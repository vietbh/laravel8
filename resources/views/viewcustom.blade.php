<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>Hello, {{ session('nickname')}}</p>
    <form action="{{route('test.store')}}" method="post">
        @csrf
        <div>
            <label for="name">Name</label><br/>
            <input type="text" name="name" id="name" value="{{old('name')}}"/>
        </div>
        <label for="description">Description</label><br/>
        <textarea name="description" id="description" cols="30" rows="10">{{old('description')}}</textarea>
        <div>
            <button type="submit">Sent</button>
        </div>
    </form>
</body>
</html>
