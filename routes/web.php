<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('client')->name('admin.')->group(function () {
    Route::get('/home/', [\App\Http\Controllers\Client\HomeController::class, 'index'])->name('home');
    Route::get('/user/{user}', [\App\Http\Controllers\Client\UserController::class, 'index'])->name('user');
});


Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');


Route::prefix('/test')->name('test.')->group(function () {
    Route::post('/store', [\App\Http\Controllers\TestController::class, 'store'])
        ->name('store');
    Route::get('/', [\App\Http\Controllers\TestController::class, 'show'])
        ->name('show');
});


Route::match(['get', 'post'], '/demo', function () {
    echo "hello";
});

Route::get('/checkage/{age}', function () {
    echo '<h1>Xin chào</h1>';
})->middleware('custom');

Route::get('/post', function () {
    echo "successfully";
})->middleware('Role:editor');


Route::get('/data', function () {
    //    session()->put('buc','condai');
    //    return redirect()->action([\App\Http\Controllers\TestController::class,'show']);
    //    return redirect()->away('https://google.com');
    return redirect('/test')->with('nickname', 'Hoang Viet');
});

Route::get('/data2', function () {
    //    \Illuminate\Support\Facades\Session::flush();
    dd(\Illuminate\Support\Facades\Session::get('buc'));
});

Route::get('/dowloand', function () {
    return response()->download('../storage/app/public/demo.txt');
});

Route::get('/stream', function () {
    return response()->streamDownload(function () {
        echo 'Bui Hoang Viet';
    }, 'users.txt');
});

Route::get('/caps/{str}', function ($str) {
    dd(response()->caps($str));
});

Route::get('/child', function () {
    return view('child');
});
Route::get('/create', function () {
    return \Illuminate\Support\Facades\URL::signedRoute('change_password', ['user' => 1]);
});

Route::get('change_password/{user}', function (Request $request) {
    if (!$request->hasValidSignature()) {
        dd('Not pass');
    }
})->name('change_password');


Route::post('/post', [\App\Http\Controllers\TestController::class, 'store'])->name('post.create');

Route::get('/post/create', function () {
    return view('post');
});

Route::get('/exceiption', function () {
    throw new Exception('Error roi');
});

Route::get('/logging', function () {
    \Illuminate\Support\Facades\Log::warning('Ngay mai la thu 2');
});

Route::get('/artisan', function () {
    \Illuminate\Support\Facades\Artisan::call('mail:send', [

    ]);
});

Route::get('/event', function () {
    event(new \App\Events\MessageNotification('This is broadcast'));
});

Route::get('/listen', function () {
    return view('listen');
});

Route::get('/collection', function () {
    collect([1, 2, 3, 4])->firstOrFail(function ($value, $key) {
        return $value > 5;
    });

});

Route::get('/test/flash', [\App\Http\Controllers\TestFlashController::class, 'flash']);

Route::get('/test', function () {
    $redis = \Illuminate\Support\Facades\Redis::connection();
    $redis->del('vietkone@gmail.com');
//    dd(1);
});

//Route::get('/sendmail', [\App\Http\Controllers\EmailController::class,'sendMail']);

Route::get('/import', [\App\Http\Controllers\ExcelController::class,'showForm']);
Route::post('/import', [\App\Http\Controllers\ExcelController::class, 'import'])
    ->name('excel.store');

require __DIR__ . '/auth.php';
