<?php
namespace viettqt\firstpackage;

use Illuminate\Support\ServiceProvider;
use viettqt\firstpackage\Http\controllers\TimezonesController;

class FirstPackageServiceProvider extends ServiceProvider {
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'timezones');

        $this->publishes([
            __DIR__ . '/resources/views' => base_path('resources/views'),
        ]);
    }
    public function register()
    {
        include __DIR__ . '/routes/web.php';
        $this->app->make(TimezonesController::class);
    }
}
