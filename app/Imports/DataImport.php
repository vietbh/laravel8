<?php

namespace App\Imports;

use App\Jobs\SaveDataDB;
use App\Jobs\SendMail;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DataImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            Bus::batch([
                new SaveDataDB($row),
                new SendMail($row)
            ])->then(function (Batch $batch) use ($row) {
            })->catch(function (Batch $batch) {
                Log::error('failed queue');
            })->dispatch();
        }

        View::share('users', $rows->toArray());
    }
}
