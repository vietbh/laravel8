<?php

namespace App\Providers;
use App\Http\Middleware\Role;
use App\Models\User;
use App\Services\Book;
use App\Services\Trans;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Interfacc\Logger;
use Interfacee\ErrorLogger;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->when(Book::class)
//                    ->needs('$id')
//                    ->give(1);
//        $this->app->singleton(
//            Logger::class,
//                    ErrorLogger::class);

//        $this->app->extend(User::class, function ($user){
//            return new \App\Models\DecoratedUser($user);
//        });
       $this->app->when(\App\Http\Controllers\TestFlashController::class)
            ->needs(\App\Contracts\USB::class)
            ->give(\App\Contracts\Support\Flash::class);
        $this->app->when(\App\Http\Controllers\TestPrinterController::class)
            ->needs(\App\Contracts\USB::class)
            ->give(\App\Contracts\Support\Printer::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Artisan::command('email:send {id*}', function ($id){
           Log::warning("user log command: {$id[0]}");
        })->purpose('tesst command closure');
    }
}
