<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            'profile', 'App\Http\View\Composers\ProfileComposer'
        );

        View::composer('dashboard2', function ($view) {
            $view->with('name', 'viet');
        });
    }
}
