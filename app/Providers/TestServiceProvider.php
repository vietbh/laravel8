<?php

namespace App\Providers;

use App\Services\Book;
use App\Services\Trans;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;

class TestServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    protected $defer = false;
    public function register()
    {
        $this->app->bind(Trans::class, function($app){
            return new Book();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function provides()
    {
        return [
            Trans::class
        ];
    }
}
