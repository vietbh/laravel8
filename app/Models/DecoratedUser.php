<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;

class DecoratedUser extends User
{
    use HasFactory;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function decoratedTest()
    {
        return 'Decorated: ' . $this->user->test();
    }
}
