<?php

    namespace App\Http\View\Composers;

    use Illuminate\View\View;

    class ProfileComposer
    {
        public function compose(View $view)
        {
            $view->with('name', 'Bùi Hoàng Việt');
        }
    }
