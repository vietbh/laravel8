<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Services\Book;
use App\Services\Trans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Interfacee\ErrorLogger;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cookie;

class TestController extends Controller
{
    public function __construct()
    {

    }

    public function show(Request $request)
    {
        return response()
                ->json([
                    'name'=>'viet',
                    'city'=>'Ha Noi'
                ]);
    }


    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'bail|required|min:5'
        ],[
            'name.required'=>'Ten khong duoc de trong'
        ]);
    }


}
