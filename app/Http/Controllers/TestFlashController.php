<?php

namespace App\Http\Controllers;

use App\Contracts\USB;
use Illuminate\Http\Request;

class TestFlashController extends Controller
{
    public function __construct(USB $flash){
        $this->flash = $flash;
    }

    public function flash(){
        return $this->flash->start();
    }

}
