<?php

namespace App\Http\Controllers;

use App\Imports\DataImport;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function import(Request $request)
    {
        Excel::import(new DataImport, $request->file('file-excel'));

        return view('pages.importSuccess');
    }

    public function showForm()
    {
        return view('pages.importExcel');
    }
}
