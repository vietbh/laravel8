<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if($role != 'editor'){
            return response('Ban khong co quyen truy cap');
        }
        return $next($request);
    }

    public function terminate($request, $response)
    {
//        DB::table('users')->insert([
//            'name'=>'viet3',
//            'email'=>'vieteeaa@gmail.com',
//            'password'=>'qqrereworwoei'
//        ]);
        session()->put('buc2','condai2');
    }
}
