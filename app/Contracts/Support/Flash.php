<?php
namespace App\Contracts\Support;

use App\Contracts\USB;

class Flash implements USB{

    public function start()
    {
        return 'U Disk Open Work';
    }
    public function stop()
    {
        return 'U Disk End Work';
    }
}
