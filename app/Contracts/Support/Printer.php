<?php

namespace App\Contracts\Support;

use App\Contracts\USB;

class Printer implements USB{

    public function start()
    {
        return 'Printer on';
    }
    public function stop()
    {
        return 'Printer End of Work';
    }
}
