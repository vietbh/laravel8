<?php
namespace App\Contracts;

interface USB{
    public function start();
    public function stop();
}

