<?php
    namespace App\Services;
    class Trans{
        private $text;

        public function __construct()
        {
        }

        public function text()
        {
            return $this->text;
        }

        public function handle()
        {
            return app(Trans::class)->text();
        }

        public function set($text)
        {
            $this->text = $text;
        }

    }
?>
